from os.path import abspath, dirname, join
from sys import path
path.append(join(abspath(dirname(__file__)), 'vendor'))

import scrapy

def lambda_handler(event, context):
	print(f"Scrapy {scrapy.__version__} from {scrapy.__file__}")
