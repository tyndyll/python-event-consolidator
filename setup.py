from setuptools import setup

setup(
    name='event_consolidator',
    version='0.1',
    description='Helping organizers schedule events compatibly',
    url='https://gitlab.com/openindustry/python-event-consolidator',
    packages=[
        'event_consolidator',
        'event_consolidator.middlewares'
    ],
    install_requires=[
        'scrapy',
        'oauthlib'
    ]
)
